# haproxy
#
# Meta-state to fully setup haproxy on debian. (or any other distro that has haproxy in their repo)

{% from "haproxy/map.jinja" import haproxy with context %}
{% if salt['pillar.get']('haproxy:selinux_state') %}
{{ salt['pillar.get']('haproxy:selinux_state') }}:
# We use cmd here to not be forced to install policycoreutils-python selinux-policy-targeted
# Ref: https://groups.google.com/forum/#!topic/salt-users/-9prMnKSnSE
  cmd.run:
    - name: setenforce {{ salt['pillar.get']('haproxy:selinux_state') }}
    - unless: getenforce|grep {{ salt['pillar.get']('haproxy:selinux_state') }}
    - order: first
{% endif %}
include:
{%- set haproxy_items = salt['pillar.get']('haproxy:include', []) %}
{%- for item in haproxy_items %}
  - {{ item }}
{%- endfor %}
  - haproxy.install
  - haproxy.service
  - haproxy.config
{% if haproxy.exporter_enabled %}
  - haproxy.exporter
{% endif %}
